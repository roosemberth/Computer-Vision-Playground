//
// Created by roosemberth on 17.01.16.
//

#include <vector>
#include <stdexcept>
#include "videoDevices.hpp"

extern "C" {
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/videodev2.h>
}

#include "SystechDefinitions.h"

// I find this ugly but makes function signatures more readable
using video::videoCapture;
using video::streamFormat;
using std::string;

videoCapture::videoCapture(const string device, const streamFormat& sfmt):
        videoDevice(device, sfmt) {
    // TODO: Initialize Buffers Maybe?
}
