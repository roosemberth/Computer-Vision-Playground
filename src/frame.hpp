//
// Created by roosemberth on 30.01.16.
//

#ifndef COMPUTER_VISION_PLAYGROUND_FRAME_HPP
#define COMPUTER_VISION_PLAYGROUND_FRAME_HPP

#include <vector>
#include <algorithm>
#include <stdexcept>
#include "streamFormat.hpp"

extern "C"{
#include <linux/videodev2.h>
};

using video::streamFormat::fourCC_t;

namespace video {
    class frame{
      private:
        // Mark if data was malloc'd by an internal function so we can free
        // it on destructor
        bool internallyAllocated = false;

        inline bool frameIsValid() const;
        inline void setNPlanes(uint32_t nPlanes);
        void allocateDataBuffers();

        uint32_t            dataPlanes; // Number of data planes
      public:
        std::vector<void *> data;       // A pointer for each of the planes
        std::vector<size_t> usedSize;   // used size on each of the buffers
        std::vector<size_t> dataSize;   // (allocated) buffer size

        streamFormat        sFmt;

        /**
         * The simple constructor:
         *  single plane, raw RGB 888, usedsize = datasize = height*width*3
         */
        frame(uint32_t height, uint32_t width);

        /**
         * The not-so-simple constructor
         *  this will build a custom frame object, arguments can't be null
         */
        frame(uint32_t dataPlanes, const std::vector<void *> &data,
              const std::vector<size_t> &usedSize,
              const std::vector<size_t> &dataSize, const streamFormat &sfmt) :
                dataPlanes(dataPlanes), data(data), usedSize(usedSize),
                dataSize(dataSize), sFmt(sfmt) {
            if (!frameIsValid()) throw std::invalid_argument("Invalid data!");
        }

        /**
         * The based constructor
         *  Historically speaking;
         *  this constructor is the reason of the existence of this class
         */
        frame(v4l2_buffer v4l2Buffer, streamFormat sfmt);

        ~frame();

    };
}

#endif //COMPUTER_VISION_PLAYGROUND_FRAME_HPP
