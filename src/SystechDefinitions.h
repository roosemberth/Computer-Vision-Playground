/**
 * SystechDefinitions
 *
 * You can call this file "misc"
 *
 * While coding this file I noticed that is should not by all means go to
 * production code, so before upstreaming any code using this header it must
 * be refactored to use upstream project framework.
 */
#include <stdio.h>

#define DEBUG_LEVEL_LOOP        0x1F    // 0x11111b
#define DEBUG_LEVEL_INFO		0x0F	// 0x01111b
#define DEBUG_LEVEL_WARN		0x07	// 0x00111b
#define DEBUG_LEVEL_ERROR		0x03	// 0x00011b
#define DEBUG_LEVEL_CRIT		0x01	// 0x00001b

#ifndef DebugLevel
  #define DebugLevel DEBUG_LEVEL_WARN
#endif


// Debug Macros
#define DEBUG_LOOP(...) if (DebugLevel >= DEBUG_LEVEL_LOOP) do{             \
    printf("[Loop    ]:  %s(%d): ", __FUNCTION__, __LINE__);                \
    printf(__VA_ARGS__);                                                    \
    printf("\n");                                                           \
    } while(0)

#define DEBUG_INFO(...) if (DebugLevel >= DEBUG_LEVEL_INFO) do{             \
    printf("[Info    ]:  %s(%d): ", __FUNCTION__, __LINE__);                \
    printf(__VA_ARGS__);                                                    \
    printf("\n");                                                           \
    } while(0)

#define DEBUG_WARN(...) if (DebugLevel >= DEBUG_LEVEL_WARN) do{             \
    printf("[Warning ]:  %s(%d): ", __FUNCTION__, __LINE__);                \
    printf(__VA_ARGS__);                                                    \
    printf("\n\t\tFrom File: %s\n", __FILE__);                              \
    } while(0)

#define DEBUG_ERROR(...) if (DebugLevel >= DEBUG_LEVEL_ERROR) do{           \
    printf("[Error   ]:  %s(%d): ", __FUNCTION__, __LINE__);                \
    printf(__VA_ARGS__);                                                    \
    printf("\n\t\tFrom File: %s\n", __FILE__);                              \
    } while(0)

#define DEBUG_CRIT(...) if (DebugLevel >= DEBUG_LEVEL_CRIT) do{             \
    printf("[Critical]:  %s(%d): ", __FUNCTION__, __LINE__);                \
    printf(__VA_ARGS__);                                                    \
    printf("\n\t\tFrom File: %s\n", __FILE__);                              \
    } while(0)

#include <stdexcept>
#include <cstdio>
#if defined(__cplusplus)
#define FATAL_ERROR(msg...) do{                                             \
    DEBUG_CRIT(msg);                                                        \
    /* Sorry, limited to 100 chars... */                                    \
    char buff[101];                                                         \
    snprintf(buff, sizeof(buff), msg);                                      \
    throw std::runtime_error(std::string(buff));                            \
} while(0)
#else
#define FATAL_ERROR(msg...) do{                                             \
    DEBUG_ERROR(msg);                                                       \
    assert(0);                                                              \
} while(0)
#endif

