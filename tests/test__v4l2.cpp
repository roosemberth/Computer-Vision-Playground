//
// Created by roosemberth on 15.01.16.
//

#include "gtest/gtest.h"
#include <iostream>

#include "../src/SystechDefinitions.h"
#include "../src/v4l2-devices.hpp"

#define TEST_VIDEO_DEVICE "/dev/videoTest"

using namespace std;
using namespace v4l2;

TEST(v4l2_device, static_getValidFormats) {
    vector<streamFormat> availableFormats =
      v4l2Device::getValidFormats(TEST_VIDEO_DEVICE);

    cout << "Got " << availableFormats.size() << " " <<
            "formats from v4l2Device::getValidFormats call" << endl;
    GTEST_ASSERT_GT(availableFormats.size(), 0);

    unsigned int ctr = 0;
    cout << "Listing available formats... " << endl;
    for (streamFormat sfmt : availableFormats)
        cout << "Format " << setw(2) << ctr++ << ": " << sfmt.str() << endl;

    cout << endl;
}

TEST(v4l2_device, static_isValidFormat){
    vector<streamFormat> availableFormats =
      v4l2Device::getValidFormats(TEST_VIDEO_DEVICE);

    std::srand((unsigned int) std::time(0));
    streamFormat testFmt =
        availableFormats.at(random()%(availableFormats.size()-1));
    cout << "Testing with format " << testFmt.str() << endl << endl;

    GTEST_ASSERT_EQ(v4l2Device::isValidFormat(TEST_VIDEO_DEVICE, testFmt), true);

    testFmt.height = 0;
    testFmt.width = 0;

    GTEST_ASSERT_EQ(v4l2Device::isValidFormat(TEST_VIDEO_DEVICE, testFmt), false);
}

TEST(v4l2_device, static_getMaxResolutionFormat){
    vector<streamFormat> maxResolutionFormats =
            v4l2Device::getMaxResolutionFormat(TEST_VIDEO_DEVICE);

    GTEST_ASSERT_NE(maxResolutionFormats.size(), 0);

    cout << "Found " << maxResolutionFormats.size() <<
            " Max Resolutions: " << endl;
    for (streamFormat sfmt : maxResolutionFormats)
        cout << "\t" << sfmt.str() << endl;
}

TEST(v4l2_device, streamControl){
    cout << "Opening test device" << endl;
    v4l2Device captureGenericObject(TEST_VIDEO_DEVICE);
    GTEST_ASSERT_EQ(captureGenericObject.startStreaming(), EPIPE);
    GTEST_ASSERT_EQ(captureGenericObject.stopStreaming(), 0);
    /**
     * Nothing else to test as this class is supposed to be extended by
     * videoCapture and videoDecoder classes
     */
}
