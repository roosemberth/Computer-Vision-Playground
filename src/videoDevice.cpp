//
// Created by roosemberth on 10.01.16.
//

#include <assert.h>
#include <exception>
#include <array>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include "videoDevices.hpp"

extern "C" {
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/videodev2.h>
}

#include "SystechDefinitions.h"

// I find this ugly but makes function signatures more readable
using video::videoDevice;
using video::streamFormat;
using std::string;

videoDevice::videoDevice(const string device): fd(-1) {
    assert(device.length()>0);
    std::vector<streamFormat> maxResolutions =
            getMaxResolutionFormat(device);
    assert(maxResolutions.size()>0);
    construct(device, maxResolutions.at(0));
};
videoDevice::videoDevice(const string device, const streamFormat& sfmt): fd(-1) {
    assert(device.length()>0);
    assert(isValidFormat(device, sfmt));
    construct(device, sfmt);
};
videoDevice::~videoDevice() {
    if (fd>=0){
        close(fd);
    }
}

void
videoDevice::construct(const string device, streamFormat sfmt) {
    dev = std::string(device);
    // No sanity check as internal functions must sanitize their inputs!
    if ((fd = open(dev.c_str(), O_RDWR | O_NONBLOCK)) < 0)
        FATAL_ERROR("Failed opening V4L2 Interface: %s", dev.c_str());

    deviceCapabilities = {};

    if (ioctl(fd, VIDIOC_QUERYCAP, &deviceCapabilities) < 0)
        FATAL_ERROR("Failed quering VIDEOC_QUERYCAP: %d", errno);

    if (!(deviceCapabilities.capabilities & V4L2_CAP_VIDEO_CAPTURE))
        FATAL_ERROR("%s is not a video capture dev!", device.c_str());

    if (!(deviceCapabilities.capabilities & V4L2_CAP_STREAMING))
        FATAL_ERROR("%s can not stream", dev.c_str());

//  if (!(devCaps.capabilities & V4L2_CAP_READWRITE))
//      FATAL_ERROR("%s can not accept I/O", dev.c_str());

    // Set stream format (device type dependant)
    this->setStreamFormat(sfmt);

    /**
     * From this point should be ready to enqueue and dequeue buffers
     * and later call {start,stop}Streaming methods...
     */
}

void
videoDevice::setStreamFormat(streamFormat &sfmt) {
    struct v4l2_format format  = {0};
    format.type                = sfmt.frmDescType;
    format.fmt.pix.pixelformat = sfmt.pixelFormat;
    format.fmt.pix.width       = sfmt.width;
    format.fmt.pix.height      = sfmt.height;
    format.fmt.pix.field       = V4L2_FIELD_ANY;

    if (ioctl(fd, VIDIOC_S_FMT, &format) < 0)
        FATAL_ERROR("Failed setting streaming format: %d,\nFormat: %s",
                    errno, sfmt.str().c_str());

    DEBUG_INFO("PixelFmt Field Order Recieved: %d", format.fmt.pix.field);

    if (format.fmt.pix.width != sfmt.width ||
        format.fmt.pix.height != sfmt.height) {
        DEBUG_WARN("Effective stream size is different than requested size");
        sfmt.width  = format.fmt.pix.width;
        sfmt.height = format.fmt.pix.height;
    }
    this->sfmt = streamFormat(sfmt);
}

int
videoDevice::startStreaming() {
    if (queuedBuffers.size()==0){
        DEBUG_WARN("Attempt to Start Streaming without enqueuing buffers!");
        return EPIPE;
    }
    if (queuedBuffers.size()<minBuffersInQueue){
        DEBUG_ERROR("Attempt to Start Streaming with a small queue");
        return EPIPE;
    }
    DEBUG_INFO("Starting Capture Queue on Device %s", dev.c_str());
    int ret = ioctl(fd, VIDIOC_STREAMON);
    if (ret == 0){
        streamOn = true;
        return ret;
    };
    return errno;
}

int
videoDevice::stopStreaming() {
    if (!streamOn)
        return 0;
    DEBUG_INFO("Stopping Capture Queue on Device %s", dev.c_str());
    int ret = ioctl(fd, VIDIOC_STREAMOFF);
    if (ret ==0) {
        streamOn = false;
        return ret;
    }
    return errno;
}

int
videoDevice::enqueueBuffer(struct v4l2_buffer &buffer) {
    // TODO: add the buffer to temp or queue, then call ioctl
    return 0;
}

struct video::v4l2_buffer*
videoDevice::dequeueBuffer() {
    /**
     * TODO: Query video for a new frame:
     * If there are new frames:
     *      enqueue lastCapture buffer,
     *      put the new buffer on its place,
     *
     */
    return lastCapture;
}

const std::vector<streamFormat>
videoDevice::getValidFormats(const string dev) {
    std::vector<streamFormat> validFormats;

    int fd = open(dev.c_str(), O_RDWR);

    if (fd < 0)
        FATAL_ERROR("Failure to Open Video Device %s: %d", dev.c_str(), fd);

    struct v4l2_fmtdesc     fmtDesc = {};
    struct v4l2_frmsizeenum frmSize = {};
    streamFormat *sfmt              = nullptr;

    for (v4l2_buf_type fmtBuffType : std::array<v4l2_buf_type, 4>{
            V4L2_BUF_TYPE_VIDEO_CAPTURE,
            V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE,
            V4L2_BUF_TYPE_VIDEO_OUTPUT,
            V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE,
        }) {
        fmtDesc.type = fmtBuffType;
        fmtDesc.index = 0;

        DEBUG_LOOP("Querying %s frame type",
                   streamFormat::readFrameType(fmtDesc.type).c_str());

        while (ioctl(fd, VIDIOC_ENUM_FMT, &fmtDesc) >= 0){
            DEBUG_LOOP("ioctl: VIDIOC_ENUM_FMT = %s",
                   streamFormat::readPixelFormat(fmtDesc.pixelformat).c_str());
            frmSize.pixel_format = fmtDesc.pixelformat;
            frmSize.index = 0;
            while (ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &frmSize)>=0){
                DEBUG_LOOP("ioctl: VIDIOC_ENUM_FRAMESIZES = %d", frmSize.index);
                frmSize.index++;
                sfmt              = new streamFormat;
                sfmt->frmDescType = fmtDesc.type;
                sfmt->pixelFormat = fmtDesc.pixelformat;
                if (frmSize.type == V4L2_FRMSIZE_TYPE_DISCRETE){
                    sfmt->height  = frmSize.discrete.height;
                    sfmt->width   = frmSize.discrete.width;
                } else if (frmSize.type == V4L2_FRMSIZE_TYPE_STEPWISE){
                    sfmt->height  = frmSize.stepwise.max_height;
                    sfmt->width   = frmSize.stepwise.max_width;
                } else {
                    DEBUG_WARN("Received unsupported frameSize Descriptor: %d"\
                    " Dropping descriptor...", frmSize.type);
                    delete sfmt;
                    continue;
                }
                validFormats.push_back(*sfmt);
            }
            if (frmSize.index == 0){
                DEBUG_INFO("No sizes returned for format %s (%s), "
                                   "returning zero-size format descriptor",
                           streamFormat::readPixelFormat(frmSize.pixel_format)
                                   .c_str(), errno);
                sfmt = new streamFormat;
                sfmt->frmDescType = fmtDesc.type;
                sfmt->pixelFormat = fmtDesc.pixelformat;
                sfmt->height      = 0;
                sfmt->width       = 0;
                validFormats.push_back(*sfmt);
            }

            fmtDesc.index++;
        }
        if (fmtDesc.index==0)
            DEBUG_INFO("No formats found for %s frame type, (%d)",
                       streamFormat::readFrameType(fmtDesc.type).c_str(),
                       errno);
    }
    DEBUG_INFO("Found %d formats for device %s",
               validFormats.size(), dev.c_str());

    return validFormats;
}

const bool
videoDevice::isValidFormat(const string dev, const streamFormat& vfmt) {
    std::vector<streamFormat> validFormats =
      video::videoDevice::getValidFormats(dev);
    if (validFormats.empty())
        return false;

    // I don't like this approach, any pull request will be easily welcome
    const std::vector<streamFormat>::iterator &iter =
            std::find(validFormats.begin(), validFormats.end(), vfmt);

    return (iter != validFormats.end());
}

const std::vector<streamFormat>
videoDevice::getMaxResolutionFormat(const string dev) {
    std::vector<streamFormat> validFormats =
      getValidFormats(dev);
    std::vector<streamFormat> *maxResolutionFormats;

    if (validFormats.empty()){
        DEBUG_WARN("Device %s has no valid output resolutions!", dev.c_str());
        return *(new std::vector<streamFormat>);
    }

    int pixelArea = 0;
    for (streamFormat sfmt : validFormats){
        if (sfmt.getPixelArea()>pixelArea){
            maxResolutionFormats = new std::vector<streamFormat>;
            pixelArea = sfmt.getPixelArea();
        }
        if (sfmt.getPixelArea()>=pixelArea){
            maxResolutionFormats->push_back(sfmt);
        }
    }

    DEBUG_INFO("Posting a valid format with a pixel area of %d", pixelArea);

    return *maxResolutionFormats;
}
