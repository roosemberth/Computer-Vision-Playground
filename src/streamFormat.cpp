//
// Created by roosemberth on 18.01.16.
//

#include <iomanip>
#include <sstream>
#include <string>

#include "streamFormat.hpp"

extern "C" {
#include <linux/videodev2.h>
}

const std::string
video::streamFormat::readPixelFormat(uint32_t PixelFormat){
    unsigned char fourCC[5]={0};            // Four bytes + null termination
    *((int *)(&(fourCC[0]))) = PixelFormat; // Souvenirs from C ages...
    return std::string((const char*)fourCC);
}

const std::string
video::streamFormat::readFrameType(uint32_t frmDescType){
    if (frmDescType==V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE){
        return "V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE";
    }else if (frmDescType==V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE){
        return "V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE";
    }else if (frmDescType==V4L2_BUF_TYPE_VIDEO_CAPTURE){
        return "V4L2_BUF_TYPE_VIDEO_CAPTURE";
    }else if (frmDescType==V4L2_BUF_TYPE_VIDEO_OUTPUT){
        return "V4L2_BUF_TYPE_VIDEO_OUTPUT";
    }
    std::ostringstream ret;
    ret << "Unknown (" << std::to_string(frmDescType) << ")";
    return ret.str();

}

const unsigned int
video::streamFormat::getPixelArea() const{
    return height * width;
}

const std::string
video::streamFormat::readResolution() const{
    std::ostringstream ret;
    ret << std::setw(5) << std::right << width << "x" <<
    std::setw(5) << std::left << height;
    return ret.str();
};

const std::string
video::streamFormat::readPixelFormat() const{
    return readPixelFormat(pixelFormat);
}

const std::string
video::streamFormat::readFrameType() const{
    return readFrameType(frmDescType);
}

const std::string
video::streamFormat::str() const {
    std::ostringstream ret;
    ret <<  readPixelFormat() << " " << readFrameType() << " " <<
    readResolution() << " (" << getPixelArea() << " pixels)";
    return ret.str();
}

const bool
video::streamFormat::operator==(const streamFormat &b) const{
    return (pixelFormat == b.pixelFormat && frmDescType == b.frmDescType &&
            height == b.height && width == b.width);
}

const bool
video::streamFormat::operator!=(const streamFormat &b) const{
    return !(operator==(b));
}

const bool
video::streamFormat::operator> (const streamFormat &b) const{
    return (getPixelArea()>b.getPixelArea());
}

const bool
video::streamFormat::operator< (const streamFormat &b) const{
    return (getPixelArea()<b.getPixelArea());
}

const bool
video::streamFormat::operator>=(const streamFormat &b) const{
    return (getPixelArea()>=b.getPixelArea());
}

const bool
video::streamFormat::operator<=(const streamFormat &b) const{
    return (getPixelArea()<=b.getPixelArea());
}
