//
// Created by roosemberth on 18.01.16.
//

#ifndef COMPUTER_VISION_PLAYGROUND_STREAMFORMAT_HPP
#define COMPUTER_VISION_PLAYGROUND_STREAMFORMAT_HPP

#include <string>

namespace video {
    class streamFormat {
    public:
        // a little endian ASCII Printable four (non-zero) character code
        typedef uint32_t fourCC_t;

        // Default copy constructor is fine, therefore not redefined...
        fourCC_t         pixelFormat;   // stream FourCC Code
        uint32_t         height;        // stream height
        uint32_t         width;         // stream width
        uint32_t         frmDescType;   // Single/Multi planar video capture

        static const std::string readPixelFormat(uint32_t PixelFormat);

        static const std::string readFrameType(uint32_t frmDescType);

        const unsigned int getPixelArea() const;

        const std::string str() const;

        const std::string readResolution() const;
        const std::string readPixelFormat() const;
        const std::string readFrameType() const;

        const bool operator==(const streamFormat &b) const;
        const bool operator!=(const streamFormat &b) const;
        const bool operator> (const streamFormat &b) const;
        const bool operator< (const streamFormat &b) const;
        const bool operator>=(const streamFormat &b) const;
        const bool operator<=(const streamFormat &b) const;
    };
}


#endif //COMPUTER_VISION_PLAYGROUND_STREAMFORMAT_HPP
