//
// Created by roosemberth on 30.01.16.
//

#include "frame.hpp"
#include <cassert>

extern "C" {
#include <linux/videodev2.h>
}

inline bool
video::frame::frameIsValid() const{
    return (data.size() >= dataPlanes) &&
           (dataSize.size() >= dataPlanes);
}

inline void
video::frame::setNPlanes(uint32_t nPlanes){
    dataPlanes = nPlanes;
    data.resize(dataPlanes);
    usedSize.resize(dataPlanes);
    dataSize.resize(dataPlanes);
}

void
video::frame::allocateDataBuffers() {
    assert(frameIsValid());
    for (unsigned int i = 0; i < dataPlanes; ++i){
        size_t planeSize = sizeof(char) * dataSize.at(i);
        data.at(i) = malloc (planeSize);
    }
    internallyAllocated = true;
}

video::frame::frame(uint32_t height, uint32_t width) {
    assert(height > 0 && width > 0);
    dataPlanes = 1;


    usedSize[0] = height * width * 3;
    dataSize[0] = height * width * 3;
    allocateDataBuffers();

    // Yes, I know this is some sort of cheating, but will make other
    // functions happy.
    sFmt.frmDescType = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    sFmt.pixelFormat = V4L2_PIX_FMT_RGB24;
    sFmt.height      = height;
    sFmt.width       = width;
}

video::frame::frame(v4l2_buffer v4l2Buffer, streamFormat sfmt): sFmt(sfmt){
        // If it's NOT multiplanar
        if (!V4L2_TYPE_IS_MULTIPLANAR(v4l2Buffer.type)){
            setNPlanes(1);
            data.at(0) = (void *) v4l2Buffer.m.userptr;
            usedSize.at(0) = v4l2Buffer.bytesused;
            dataSize.at(0) = v4l2Buffer.length;
        } else {
            setNPlanes(v4l2Buffer.length);
            for (unsigned int i = 0; i<dataPlanes; ++i){
                data.at(i) = (void *) v4l2Buffer.m.planes[i].m.userptr;
                usedSize.at(i) = v4l2Buffer.m.planes[i].bytesused;
                dataSize.at(i) = v4l2Buffer.m.planes[i].length;
            }
        }
}

video::frame::~frame(){
    // Free memory if allocated by an internal function
    if (internallyAllocated) {
        std::for_each(data.begin(), data.end(), [](auto &elem) {
            free(elem);
        });
    }
}
