//
// Created by roosemberth on 09.01.16.
//

#ifndef COMPUTER_VISION_PLAYGROUND_V4L2_DEVICES_HPP
#define COMPUTER_VISION_PLAYGROUND_V4L2_DEVICES_HPP

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include "streamFormat.hpp"
#include "frame.hpp"

namespace video {
// videoDevice.cpp
    class videoDevice {
      private:
        void construct(const std::string device, streamFormat sfmt);

      protected:
        int                    fd;
        std::string            dev;
        struct v4l2_capability deviceCapabilities;
        bool                   streamOn;
        streamFormat           sfmt;
        frame                  *lastCapture;
        std::vector<frame *>   queuedBuffers;
        static unsigned int    minBuffersInQueue = 2;  // +1 in lastCapture
      public:
        static const unsigned int getMinBuffersInQueue() {
            return minBuffersInQueue;
        }

        /**
         * v4l2_device:
         * \brief       Creates a v4l2_device from a device path
         *
         * \param dev   V4L2 Device Path (ie. "/dev/video7")
         */
        videoDevice(const std::string dev);
        videoDevice(const std::string dev, const streamFormat &sfmt);
        ~videoDevice();

        /**
         * start/stop Streaming
         * \brief       starts/stops the video stream
         * Needs at least minBuffersInQueue buffers in each queue to succeed.
         *
         * \return      0 on success
         *              error value returned by video ioctl VIDIOC_STREAM{ON,OFF}
         */
        virtual int startStreaming();
        virtual int stopStreaming();

        /**
         * getValidFormats:
         * \brief       returns the device's valid formats
         * If the specified device supports a given format but does not provide
         * any valid resolution, a null-size format will be present for this
         * format.
         *
         * \param dev   V4L2 Device Path (ie. "/dev/video7")
         * \return      std::vector<streamFormat> containing valid formats.
         */
        static const std::vector<streamFormat>
                getValidFormats(const std::string dev);

        /**
         * isValidFormat:
         * \brief       checks if a given format is valid.
         *
         * \param dev   V4L2 Device Path (ie. "/dev/video7").
         * \param vfmt  streamFormat to check.
         * \return      true is is a valid format
         *              false otherwise
         */
        static const bool
                isValidFormat(const std::string dev, const streamFormat &fmt);

        /**
         * getMaxResolutionFormat:
         * \brief       Retrieves the max resolution (equal area) formats
         *
         * \param dev   V4L2 Device Path (ie. "/dev/video7")
         * \return      vector containing the max resolution (equal area) formats
         */
        static const std::vector<streamFormat>
                getMaxResolutionFormat(const std::string dev);

        /**
         * setStreamFormat:
         * \brief       Sets the specified format on the stream
         * Changes by the driver in the proposed format, will be reflected in
         * the argument object
         *
         * \param sfmt  desired stream format
         */
        virtual void setStreamFormat(streamFormat &sfmt);

        /**
         * TODO: Doc
         */
        int enqueueFrame(frame frm);

        /**
         * TODO: Doc
         */
        frame &getFrame();
    };

// videoCapture.cpp
    class videoCapture : public videoDevice {
      public:
        videoCapture(const std::string device);
        videoCapture(const std::string device, const streamFormat &sfmt);
    };

// videoDecoder.cpp
    class videoDecoder : public videoDevice {
      public:
        videoDecoder(const std::string device, const streamFormat &sfmt);

        static const std::vector<streamFormat>
                getValidFormats(const std::string dev);

        void    setStreamFormat(streamFormat &sfmt);
    };
}   // namespace video

#endif //COMPUTER_VISION_PLAYGROUND_V4L2_DEVICES_HPP
